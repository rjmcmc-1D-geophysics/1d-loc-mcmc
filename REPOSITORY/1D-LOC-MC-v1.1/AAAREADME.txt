#
# 1D-LOC-MCMC project:
#
# THIS REPOSITORY CONTAINS THE SOURCE CODE for publication:
#
#    AUTHORS: Riva, F., N. Piana Agostinetti, S. Marzorati and C. Horan
#    TITLE: The micro-seismicity of Co. Donegal (Ireland): Defining baseline seismicity in a region of slow 
#           lithospheric deformation
#    JOURNAL: Terra Nova  
#    YEAR: 2024 
#    DOI: 10.1111/ter.12691
#
#   The repository contains a FORTRAN source code which implements the algorithm presented in the 
#   publication, a Markov chain Monte Carlo algorithm for the location of a seismic event in an half-space. 
#   All innovation, formulas and details are given in the publication. The code can be used
#   to reproduce all the results obtained in the paper, for one of the real-world cases presented in the 
#   manuscript.
#
#  1D-LOC-MCMC is free software: you can redistribute it and/or modify it under the terms of the GNU 
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  1D-LOC-MCMC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
#  License for more details.
#
#  You should have received a copy of the GNU General Public License along with the code.  If not, 
#  see <http://www.gnu.org/licenses/>
#
#  See also <https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc/> for more details on the code, updates
#  and new versions. 
#
#  For further information write to:  Nicola Piana Agostinetti, email: nicola.pianaagostinetti@unimib.it
#
#  Created: 2024-07-01
#  Copyright: 2024-2034
#    Nicola Piana Agostinetti 
#
#   
# ----------------------------------------------------------------------------------------------------------
#
#   1. How to compile
#
#    The FORTRAN code can be compiled using GFORTRAN. At present, the code makes use of the intrinsic 
#    subroutine RANDOM_NUMBER, for generating the sequence of Pseudo-random values between 0 and 1,
#    needed to propose candidate models in the RjMcMC sampling and to accept/reject them. This intrinsic 
#    subroutine has been tested for GFORTRAN. Any other compiler can be used, but the generation of random
#    number should be adjusted following the native random number generator.
#
#    WARNING:
#
#    The original code made use of the RAN3 subroutine, from Numerical Recipe. It has been replaced. Thus,
#    results from the new version of the code are not exactly the same presented in the publication, but
#    they must be statistically consistent.
#
#
#    To compile the source code enter the src dir, i.e.:
#
#      cd CODE-v1.1/src
#
#    Edit the compile-all file, to set the GFORTRAN path, e.g.:
#
#      vi  compile-all
#
#    Run compile-all script:
#
#      ./compile-all 
#
# 
# ----------------------------------------------------------------------------------------------------------
#
#   2. How to run the test 
#
#    The directory "data" contains the data and all necessary information (e.g. station coordinates and
#    prior information) to generate the results presented in Figure 6 in the above publication. Such test
#    is a template and all the results presented can be reproduces as well, upon request of the seismic
#    data to the authors.
#
#    To run the test, after compiling the code, enter the data dir: i.e.:
#
#      cd CODE-v1.1/data
#
#      ../bin/locate_events_rjmcmc
#
# 
# ----------------------------------------------------------------------------------------------------------
#
#   3. How to plot results
#
#    (Required software: Jupyter project)
#
#   The Jupyter Notebook "plot-results.ipynb" has been added to show how to handle the results of a computation
#   and to reproduce Figure 6 in the publication. All notebook/scripts used to compute the Figures in the
#   publication are available upon request.
# 
#   To run the notebook, you need to install Jupyter see: <https://jupyter.org/>. 
#
#   To plot the Figure 6:
#
#     1/ Open Jupyter Notebook or Lab.
#
#     2/ Navigate to the current directory and run the notebook plot-results.ipynb
#
#
#
# ----------------------------------------------------------------------------------------------------------

