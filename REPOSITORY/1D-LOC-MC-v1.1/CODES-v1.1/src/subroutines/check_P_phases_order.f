c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



	subroutine check_P_phases_order(OUT_OF_SEQ)

	implicit none

	include '../mcmc.param'
	include '../mcmc.common'

	integer OUT_OF_SEQ

	integer iph, i_ordered_oph(maxstat), i_ordered_sph(maxstat), nph
	real*4  synph(maxstat), obsph(maxstat)

	nph=0
	do iph=1,nphases
	  if(orig_ph(iph,1).gt.0)then
            nph=nph+1
	    obsph(nph)=mod__ph(iph,1)
	    synph(nph)=syn__ph(iph,1)
	    if(debug)write(lu_debug,'(a,i5,2f10.5)') '+CHECK-ORDER:', nph, synph(nph), obsph(nph)
	  endif
        enddo

	call indexx(nph,synph,i_ordered_sph)
	call indexx(nph,obsph,i_ordered_oph)

	OUT_OF_SEQ=nph
	nph=0
        do iph=1,nphases
	  if(orig_ph(iph,1).gt.0)then
	    nph=nph+1
	    if(i_ordered_oph(nph).eq.i_ordered_sph(nph))OUT_OF_SEQ=OUT_OF_SEQ-1
	    if(debug)write(lu_debug,'(a,4i5)') '+CHECK-ORDER:', nph, i_ordered_oph(nph), i_ordered_sph(nph), OUT_OF_SEQ
	  endif
        enddo

        return
        end
