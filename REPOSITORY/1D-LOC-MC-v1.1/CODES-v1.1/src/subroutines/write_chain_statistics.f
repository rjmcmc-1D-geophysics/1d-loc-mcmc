c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



	subroutine write_chain_statistics(ic,it,move,accepted,lppd,lh_norm,hparam,OUT_OF_SEQ)

	implicit none

        include '../mcmc.param'
        include '../mcmc.common'

	integer ic, it, move, accepted, OUT_OF_SEQ
	real*4  lppd, lh_norm
	real*4  hparam(2)

        write(lu_log,201) 'IC/IT/MOVE/ACC', ic,it,move,accepted, lppd,lppd0,lh_norm,lh_norm0,hparam(1),hparam(2),OUT_OF_SEQ
 201    format(a,i4,i9,2i3,4f16.4,2f10.4,i3)

	return
	end






