c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



	subroutine misfit(param,hparam,lh_norm,lppd)

	implicit none

	include '../mcmc.param'
	include '../mcmc.common'

        real*4 param(6)
	real*4 hparam(2)
	real*4 lppd, lh_norm

	integer iph


        lppd=0.0
        lh_norm=0.0

        do iph=1,nphases
          if(orig_ph(iph,1).gt.0)lppd = lppd + ( (mod__ph(iph,1)-syn__ph(iph,1)) / (std0*10.0**hparam(1)) )**2
          if(orig_ph(iph,2).gt.0)lppd = lppd + ( (mod__ph(iph,2)-syn__ph(iph,2)) / (std0*10.0**hparam(2)) )**2
	  if(debug)write(lu_debug,'(a,i5,f16.4)') ' ++SUB MISFIT:: ', iph, lppd
        enddo

        lh_norm = 1.0*(nph_p*hparam(1)+nph_s*hparam(2))*2.302585e0

        if(debug)write(lu_debug,'(a,2f16.4)') ' ++SUB MISFIT:: LPPD/LH_NORM=', lppd, lh_norm


        return
        end



