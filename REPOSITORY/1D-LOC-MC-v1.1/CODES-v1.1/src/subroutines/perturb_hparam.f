c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/


	subroutine perturb_hparam(hparam0,hparam)

	implicit none

        include '../mcmc.param'
        include '../mcmc.common'

	real*4 hparam0(2), hparam(2)
	real*4  x, x_new, xmin, xmax, sc


c
c Perturb 
c

	x=hparam0(1)
	xmin=min_hparam(1)
	xmax=max_hparam(1)
	sc=hscale(1)

	call pick_cand_value_uniform(x,xmin,xmax,sc,x_new)

	hparam(1)=x_new

        x=hparam0(2)
        xmin=min_hparam(2)
        xmax=max_hparam(2)
        sc=hscale(2)

        call pick_cand_value_uniform(x,xmin,xmax,sc,x_new)

        hparam(2)=x_new


        if(debug) write(lu_debug,'(a,i5,4f10.4)') ' PERTURB-HPARAM:: ID/HPARAM0/HPARAM :', 0, hparam0(1), hparam(1), hparam0(2), hparam(2)

	return
	end
