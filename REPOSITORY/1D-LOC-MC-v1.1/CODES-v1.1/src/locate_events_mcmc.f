c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/
c
c
c
c WORKFLOW:
c
c	Pre-processing operations:
c       * Read file with event phases
c 	* Define t0 as starting time for OT search
c       * Adjust P- and S- times according to t0
c       * Read station coordinates
c	* Starting model
c
c	MCMC:       
c	* Generate candidate
c	* Compute Likelihood --- from both P and S phases TOGETHER
c	* Apply Metropolis rule
c	* Move chain
c	* (1/100) Save current model
c
c
c ---------------------------------------------------------------------
c
c MODEL:
c
c	* Event coordinates: x,y,z,t
c	* Half-space seismic velocities: Vp, Vp/Vs
c	* Hyper-parameters for errors: 
c            hparam(1) for P-picks, hparam(2) for S-picks
c
c ---------------------------------------------------------------------
c
c
	PROGRAM LOCATE_EVENT_MCMC

	implicit none

	include 'mcmc.param'
	include 'mcmc.common'



c ------------------------- MCMC variables

	integer ntot, nsample, nchains, burn_in
	real*4  param(6), param0(6)
	real*4  hparam(2), hparam0(2)

c ------------------------- Scratch variables
	
	integer is, id, iph, istat, idum, ichain, it, OUT_OF_SEQ
	integer ip, move, accepted, iph_start, istat0

	real*4 zdum, lppd, lh_norm


	real*4 p, ran3
	
c
c ------------------------------------------------------------
c ------------------------------------------------------------
c
c  Define random seed for RANDOM_NUMBER
c
        iseed0=123456
        call init_random_number(iseed0)

c
c ------------------------------------------------------------
c ------------------------------------------------------------
c
c
c Set-up
c
c
        debug=.false.
        open(111,file='activate_debug',status='old',ERR=2002)
        debug=.true.
        write(*,*) ' __DEBUG_MODE__'
        close(111)
 2002   continue
        prior=.false.
        open(111,file='activate_prior',status='old',ERR=2003)
        prior=.true.
        write(*,*) ' __ RUNNING INTO THE PRIOR __'
        close(111)
 2003   continue



	lu_out2=998
        lu_out=98
        lu_log=99
	lu_syn=999

        open(lu_out,file='output/samples.dat',status='unknown')
        open(lu_out2,file='output/residuals.dat',status='unknown')
        open(lu_log,file='output/chain.dat',status='unknown')

        if(debug)lu_debug=97
        if(debug)open(lu_debug,file='debug',status='unknown')
c
c
c
c ------------------------------------------------------------
c ------------------------------------------------------------
c
c
c
c
c	* Read mcmc params

	open(10,file='mcmc.in',status='old')
	read(10,*)
	read(10,*) maxchains
	read(10,*) maxiter
	read(10,*) nsamples
	read(10,*)
	do id=1,6
	read(10,*)min_param(id), max_param(id), scales(id)
	enddo
	read(10,*)
	read(10,*) min_hparam(1), max_hparam(1), hscale(1)
        read(10,*) min_hparam(2), max_hparam(2), hscale(2)
	close(10)

        burn_in=int(maxiter/2)+1
	ntot=int((maxiter-burn_in-1)/nsamples)+1

	if(debug)write(lu_debug,*) ' --> Read McMC parameters ..'

c       * Read file with event phases

	nph_p=0
	nph_s=0

	open(10,file='input/ev_phases',status='old')
        read(10,*)
	read(10,*) nphases
        read(10,*)
	do iph=1,nphases
	  read(10,*) idum, ph_stat(iph), orig_ph(iph,1), orig_ph(iph,2)
	  if(orig_ph(iph,1).gt.-0.0000000001)nph_p=nph_p+1
	  if(orig_ph(iph,2).gt.-0.0000000001)nph_s=nph_s+1
	enddo
	if(debug)write(lu_debug,*) 'MCMC-EV-LOCATE:: Found: ', nph_p, nph_s, ' P- and S- phases'
	close(10)


	
c       * Define t0 as starting time for OT search -- T0 is the minimum P-arrival time

	iph_start=-1
	do iph=1,nphases
	if(orig_ph(iph,1).gt.0)then
          t0=orig_ph(iph,1)
	  iph_start=iph+1
	  goto 1111 
	endif
	enddo
	if(iph_start.lt.0)then
	  write(*,*) ' ERROR -- No P-phase found ... exit '
	  stop
        endif
 1111   continue
	do iph = iph_start, nphases
	  if(orig_ph(iph,1).gt.0.0.and.orig_ph(iph,1).lt.t0)t0=orig_ph(iph,1)
	enddo

c       * Adjust P- and S- times according to t0

	do iph=1,nphases
	  mod__ph(iph,1)=sngl(orig_ph(iph,1))
          mod__ph(iph,2)=sngl(orig_ph(iph,2))
	  if(orig_ph(iph,1).gt.0)mod__ph(iph,1)=sngl(orig_ph(iph,1)-t0)
	  if(orig_ph(iph,2).gt.0)mod__ph(iph,2)=sngl(orig_ph(iph,2)-t0)
	enddo

c	* Write out event details for plotting and post-processing steps

	open(10,file='output/event_details',status='unknown')
	write(10,'(3(a,i4),a,f24.6)') 'NSTAT',nphases,' NPH_P',nph_p,' NPH_S',nph_s,' T0', t0
	do iph=1,nphases
	  write(10,'(2i5,2f16.6)') iph, ph_stat(iph), (mod__ph(iph,id),id=1,2)
	enddo
	close(10)

c       * Read station coordinates

	open(10,file='input/stations',status='unknown')
        read(10,*)
	read(10,*) nstat
        read(10,*)
	do istat0=1,nstat
	  read(10,*) istat, (stat_pos(istat,id),id=1,3)
	enddo



c
c Start main sampling -- 
c


        do ichain=1,maxchains



c Initialize model(0): randomly extracted from prior

	do id=1,6
          p=ran3(iseed0)
          param0(id)=min_param(id)+p*(max_param(id)-min_param(id))
	enddo
	p=ran3(iseed0)
        hparam0(1)=min_hparam(1)+p*(max_hparam(1)-min_hparam(1))
        p=ran3(iseed0)
        hparam0(2)=min_hparam(2)+p*(max_hparam(2)-min_hparam(2))

	if(debug)then
	  write(lu_debug,'(a)') ' GET-INIT-MODEL::'
          write(lu_debug,'(a,6f10.4)') ' GET-INIT-MODEL: PARAM:', (param0(id),id=1,6)
          write(lu_debug,'(a,2f10.4)') ' GET-INIT-MODEL: HPARAM:', hparam0(1), hparam(2)
          write(lu_debug,'(a)') ' GET-INIT-MODEL::'
        endif

c
c
c Start MCMC
c 
c
        do it=1,maxiter

            if(debug)write(lu_debug,*)' DEBUG:: '
            if(debug)write(lu_debug,*)' DEBUG:: ITER=', it

         
	    if(mod(it,100000).eq.0) then
               write(*,*) '   Chain:', ichain, ' - Model:', it, ' out of ', maxiter
            endif
c
c       * Generate candidate
c

c Current model becomes candidate, and then it is perturbed
 
	do id=1,6
          param(id)=param0(id)
        enddo
        do ip=1,2
          hparam(ip)=hparam0(ip)
        enddo



        if(it.eq.1)then

          write(*,*) ' START Chain:', ichain, '  -- Starting model is NOT perturbed '

        else

          p=ran3(iseed0)

          if(debug)write(lu_debug,'(a,6f10.4)') ' DEBUG:: old MODEL=', (param0(id),id=1,6)
          if(debug)write(lu_debug,'(a,2f10.4)')  ' DEBUG:: old HPARAM=', hparam0(1), hparam(2)


          if(p.lt.0.2)then
            move=1
            if(debug)write(lu_debug,*)' DEBUG:: MOVE=', move, p
            call perturb_hparam(hparam0,hparam)
            if(debug)write(lu_debug,'(a,f10.4)') ' DEBUG:: NEW HPARAM=', hparam(1), hparam(2)
	  else
            move=2
            if(debug)write(lu_debug,*)' DEBUG:: MOVE=', move, p
            call perturb_model(move,param0,param)
            if(debug)write(lu_debug,*)' DEBUG:: MOVE=', move
            if(debug)write(lu_debug,'(a,6f10.4)') ' DEBUG:: NEW MODEL=', (param(id),id=1,6)
          endif

        endif


	  
c       * Compute Likelihood --- from both P and S phases TOGETHER

	call  forward(param)

	call  misfit(param,hparam,lh_norm,lppd)

c       * Apply Metropolis rule

	call  metropolis(it,lh_norm,lppd,accepted)

c       * Move chain --1

	if(accepted.eq.0)then

	  if(debug)write(lu_debug,'(a)') ' Accepted=0 -- chain does NOT move, old current is kept '

          do id=1,6
            param(id)=param0(id)
          enddo
          do ip=1,2
            hparam(ip)=hparam0(ip)
          enddo
	  do is=1,nphases
          do ip=1,2
            syn__ph(is,ip)=syn__ph0(is,ip)
          enddo
	  enddo

        endif

	  
c       * (1/nsample) Save current model

	if(mod(it,nsamples).eq.0)then

c       * Check station order (OUT_OF_SEQ flag)
          call check_P_phases_order(OUT_OF_SEQ)
c	* Write current model to file
	  call write_model(ichain,it,param,hparam)
c       * Write chain statistics to file
	  call write_chain_statistics(ichain,it,move,accepted,lppd,lh_norm,hparam,OUT_OF_SEQ)
c       * Save synthetic predictions -- SECOND HALF OF THE CHAIN
	  if(it.ge.burn_in)then
	    call save_synthetics
          endif

	endif

c       * Move chain --2

        do id=1,6
          param0(id)=param(id)
        enddo
        do ip=1,2
          hparam0(ip)=hparam(ip)
        enddo
        do is=1,nphases
        do ip=1,2
          syn__ph0(is,ip)=syn__ph(is,ip)
        enddo
        enddo




c
c END DO-loop over samples
c
	enddo

c
c END DO-loop over chains
c
	enddo


c
c Finalize
c
	call write_synthetics(ntot)
c
c
c


	stop
	end

	
