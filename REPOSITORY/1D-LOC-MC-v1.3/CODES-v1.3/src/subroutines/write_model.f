c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



        subroutine write_model(ic,it,param,hparam)

	implicit none

        include '../mcmc.param'
        include '../mcmc.common'


	real*4  param(6), hparam(2)
	integer id, ic, it, ip, is

        write(lu_out,201) '+MODfromPPD::', ic, it, (param(id),id=1,6), param(5)/param(6), hparam(1), hparam(2)
c	write(lu_out2,202) '+RESfromPPD::', ic, it, ((mod__ph(is,1)-syn__ph(is,1)), is=1,nstat ),((mod__ph(is,2)-syn__ph(is,2)), is=1,nstat )
       write(lu_out2,202) '+RESfromPPD::', ic, it, (mod__ph(1,1)-syn__ph(1,1)),(mod__ph(is,2)-syn__ph(1,2))
 201    format(a,i4,i8,10f14.4)
 202    format(a,i4,i8,2f9.4)

	return
	end






