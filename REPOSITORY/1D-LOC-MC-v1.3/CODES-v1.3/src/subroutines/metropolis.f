c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/

c
c
c       subroutine METROPOLIS
c
c-------------------------------------------------------------------------------------------
c
c INPUT:
c         im --   model index
c         lppd -- log value of the Likelihood
c
c OUTPUT:
c         accepted --   Flag for the Metropolis rule output (see below)
c
c-------------------------------------------------------------------------------------------
c
c METROPOLIS RULE OUTPUT will be:
c
c (ACCEPTED= -1) Candidate model is accepted w/ Metropolis rule (Likelihood is decreased)
c (ACCEPTED=  0) Candidate model is rejected
c (ACCEPTED=  1) Candidate model is accepted w/o Metropolis rule (Likelihood is increased)
c
c-------------------------------------------------------------------------------------------
c
c
        subroutine metropolis(im,lh_norm,lppd,accepted)

        implicit none

        include '../mcmc.param'
        include '../mcmc.common'


c
c MAIN variables

        integer im, accepted
        real*4 lh_norm, lppd


c
c Scratch variables

        real*4 alpha

c
c RAN3 variables

        real*4 p, ran3


        if(im.eq.1) alpha= 1.0
        if(im.ne.1) alpha= exp( lh_norm0 - lh_norm - 0.5d0*(lppd-lppd0) )
        if(prior)   alpha= 1.0

	p=ran3(iseed0)

        if(debug) write(lu_debug,'(a)') ' +METROPOLIS::'
        if(debug) write(lu_debug,'(a,4f16.4)') ' +METROPOLIS::', lppd, lppd0, lh_norm, lh_norm0
        if(debug) write(lu_debug,'(a,2f10.4,i5)') ' +METROPOLIS::', alpha, p, accepted
        if(debug) write(lu_debug,'(a)') ' +METROPOLIS::'

        if(alpha.ge.1.0)then

          accepted=1
          lh_norm0=lh_norm
          lppd0=lppd

        else if(alpha.lt.1.0)then

          if(alpha.ge.p)then
     
            accepted=-1
            lh_norm0=lh_norm
            lppd0=lppd

          else

            accepted=0

          endif

        endif


        return
        end



