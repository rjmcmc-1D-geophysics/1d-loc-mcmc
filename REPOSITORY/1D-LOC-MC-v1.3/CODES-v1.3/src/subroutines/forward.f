c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



	subroutine forward(param)

	implicit none

	include '../mcmc.param'
	include '../mcmc.common'

	real*4 param(6)
	real*4 dist, tt_p, tt_s

	integer id, iph

	

	do iph=1,nphases
	
	  syn__ph(iph,1)=-1000.0
          syn__ph(iph,2)=-1000.0

	  if(debug) write(lu_debug,'(a,2i5)') '+FORWARD:: STAT:', iph, ph_stat(iph)

c Compute stat-ev dist

	  dist=0.0
	  do id=1,3
	    dist=dist+(stat_pos(ph_stat(iph),id)-param(id))**2
c	    if(debug) write(lu_debug,*) '+FORWARD::',iph, ph_stat(iph),id,stat_pos(ph_stat(iph),id),param(id)
	  enddo
	  dist=sqrt(dist)
          if(debug) write(lu_debug,'(a,i5,f16.4)') '+FORWARD::', iph, dist

	  if(orig_ph(iph,1).gt.-0.000001) tt_p=dist/param(5)
	  if(orig_ph(iph,2).gt.-0.000001) tt_s=dist/(param(5)/param(6))

	  if(orig_ph(iph,1).gt.-0.000001) syn__ph(iph,1)=tt_p+param(4)
	  if(orig_ph(iph,2).gt.-0.000001) syn__ph(iph,2)=tt_s+param(4)

	  if(debug) write(lu_debug,'(a,i5,2f16.4)') '+FORWARD::', iph, syn__ph(iph,1), syn__ph(iph,2)

	enddo


	return
	end
		
