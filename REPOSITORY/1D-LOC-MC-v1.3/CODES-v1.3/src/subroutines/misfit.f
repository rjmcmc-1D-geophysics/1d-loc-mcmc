c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



	subroutine misfit(param,hparam,lh_norm,lppd)

	implicit none

	include '../mcmc.param'
	include '../mcmc.common'

        real*4 param(6)
	real*4 hparam(2)
	real*4 lppd, lh_norm
	real*4 a, b, p

	integer iph, jph, iph_min, iph_max, kind_of_phase


        lppd=0.0
        lh_norm=0.0



          do iph=1,nphases

	    do kind_of_phase=1,1

	      if(mod__ph(iph,kind_of_phase).gt.-10.0000001)then

                p = 0.
  
  	        iph_min=iph-1
	        iph_max=iph+1
	        if(iph.eq.1)iph_min=1
                if(iph.eq.nphases)iph_max=nphases

                do jph=iph_min, iph_max

                  a=10.0**(-2*(hparam(kind_of_phase))) * icov(iph,jph)
                  b=(mod__ph(jph,kind_of_phase)-syn__ph(jph,kind_of_phase))
                  p = p + a*b
                  if(debug)write(lu_debug,'(a,3i5,4f16.4)') ' ++SUB MISFIT:: ', iph,jph,kind_of_phase, a,b,p, icov(iph,jph)


                enddo

                lppd = lppd + p*(mod__ph(iph,kind_of_phase)-syn__ph(iph,kind_of_phase))
                if(debug)write(lu_debug,'(a,i5,4f16.4)') ' ++SUB MISFIT:: ', iph, mod__ph(iph,kind_of_phase), syn__ph(iph,kind_of_phase), icov(iph,iph), lppd

              endif

           enddo

         enddo

        lh_norm = 1.0*(nph_p*hparam(1)+nph_s*hparam(2))*2.302585e0

        if(debug)write(lu_debug,'(a,2f16.4)') ' ++SUB MISFIT:: LPPD/LH_NORM=', lppd, lh_norm


        return
        end



