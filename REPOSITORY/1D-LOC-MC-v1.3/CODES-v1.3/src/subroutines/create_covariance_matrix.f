


	subroutine create_covariance_matrix

c
c
c CREATE A FULL COVARIANCE MATRIX
c
c Here we use the following assumptions:
c
c (1) the correlation matrix is the same for all the traces
c (2) the correlation matrix is assumed to be exponential (or Gaussian)
c (3) all the traces have the same number of samples
c
c   Covariance matrix is created from the correlation function, assuming
c   a uniform variance for all data-points. Then, the inverse of the 
c   covariance matrix is computed using SVD. 
c
c   See also:
c    
c   Piana Agostinetti et al., 2022, JGR
c   Kobl and Lekic (2014) GJI
c   Bodin et al. (21012) JGR
c   Malinverno and Briggs (2004) GEOPHYSICS
c



	implicit none

	include '../mcmc.param'
        include '../mcmc.common'

        real*4 diag0
	real*4 cor(maxstat,maxstat)
        real*4 iden(maxstat,maxstat)



        integer ir, ic, isamp, jsamp



        if(.not.gauss)then
c
c Constructing EXPONENTIAL correlation matrix

          do ir=1,nphases
            diag0=r_corr**(ir-1)
c	    write(*,*) ' TEST- corr: ', ir, r_corr, diag0
            do ic=ir,nphases
              cor(ic-ir+1,ic)=diag0
              cor(ic,ic-ir+1)=diag0
            enddo
          enddo

        else

c Constructing GAUSSIAN correlation matrix

          do ir=1,nphases
            diag0=r_corr**((ir-1)*(ir-1))
c            write(*,*) ' TEST- corr: ', ir, r_corr, diag0
            do ic=ir,nphases
              cor(ic-ir+1,ic)=diag0
              cor(ic,ic-ir+1)=diag0
            enddo
          enddo


        endif



c
c Writing COVARIANCE matrix and computing INVERSE COVARIANCE matrix
c

            do isamp=1,nphases
              do jsamp=1,nphases
                cov(isamp,jsamp)=cor(isamp,jsamp)*std0*std0
              enddo
            enddo


            if(.not.gauss)then
c
c Constructing EXPONENTIAL INVERSE correlation matrix

	      write(*,*) ' WRITING  INVERSE covariance matrix...'

              do ir=1,nphases
                if(ir.eq.1) then
                  diag0=1.0+r_corr**2
                else if(ir.eq.2) then
                  diag0=-1.0*r_corr
                else
                  diag0=0.0
                endif

                do ic=ir,nphases
                  cor(ic-ir+1,ic)=diag0
                  cor(ic,ic-ir+1)=diag0
                enddo

              enddo

	      cor(1,1)=1.0
              cor(nphases,nphases)=1.0

              do ir=1,nphases
                do ic=1,nphases
                  icov(ir,ic)=(1.0/(1.0-r_corr**2))*(1.0/(std0*std0))*cor(ir,ic)
                enddo
              enddo

            else

c Inverting the correlation matrix using SVD

              write(*,*) ' Computing INVERSE covariance matrix...'
              call compute_inv_cov(nphases,cov,icov,iden)

            endif

            open(7778,file='output/cor.dat',status='unknown')
            do isamp=1,nphases
              write(7778,*) (cor(isamp,jsamp),jsamp=1,nphases)
            enddo
            close(7778)
	    open(7777,file='output/icov.dat',status='unknown')
            do isamp=1,nphases
              write(7777,*) (icov(isamp,jsamp),jsamp=1,nphases)
            enddo
	    close(7777)
            open(7776,file='output/cov.dat',status='unknown')
            do isamp=1,nphases
              write(7776,*) (cov(isamp,jsamp),jsamp=1,nphases)
            enddo
            close(7776)
            open(7775,file='output/iden.dat',status='unknown')
            do isamp=1,nphases
              write(7775,*) (iden(isamp,jsamp),jsamp=1,nphases)
            enddo
            close(7775)




	return
	end



