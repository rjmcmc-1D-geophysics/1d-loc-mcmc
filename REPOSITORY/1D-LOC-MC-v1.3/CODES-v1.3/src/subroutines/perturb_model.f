c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/


	subroutine perturb_model(move,param0,param)

	implicit none

        include '../mcmc.param'
        include '../mcmc.common'


	real*4  param0(6), param(6)
	real*4  x, x_new, x_min, x_max, sc
	real*4 delta_prob, min_prob, max_prob

	integer id, id_sele, move

	real*4  p,ran3

c
c Select parameter 
c
	p=ran3(iseed0)

	id_sele=-1
	delta_prob=1.0/6.0
	min_prob=0.0
	max_prob=0.0
	do id=1,6
	  min_prob=max_prob
	  max_prob=min_prob+delta_prob
	  if(min_prob.lt.p.and.p.ge.max_prob)id_sele=id
	enddo
	if(id_sele.lt.0)id_sele=6
	move=id_sele+1
	  
c
c Perturb 
c

	x=param0(id_sele)
	x_min=min_param(id_sele)
	x_max=max_param(id_sele)
	sc=scales(id_sele)

	call pick_cand_value_uniform(x,x_min,x_max,sc,x_new)

	param(id_sele)=x_new

	if(debug) write(lu_debug,'(a,i5,2f10.4)') ' PERTURB-MODEL:: ID/PARAM0/PARAM :', id_sele, param0(id_sele), param(id_sele)

	return
	end






