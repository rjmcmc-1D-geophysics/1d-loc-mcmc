c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/



        subroutine save_synthetics

	implicit none

        include '../mcmc.param'
        include '../mcmc.common'

	real*4  cur
	integer idiv, ip, is




	do is=1,nphases
	do ip=1,2
          mean_syn_ph(is,ip)=mean_syn_ph(is,ip)+syn__ph0(is,ip)
          mean_syn_ph2(is,ip)=mean_syn_ph2(is,ip)+syn__ph0(is,ip)**2
        enddo
        enddo


	return
	end






