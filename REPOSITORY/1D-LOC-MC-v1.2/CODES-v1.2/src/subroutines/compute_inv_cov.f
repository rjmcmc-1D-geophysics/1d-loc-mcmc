
      subroutine  compute_inv_cov(nsamp,cov,icov,ident)

        implicit none

        include '../mcmc.param'

        integer nsamp, nsamp2, isamp, jsamp, ksamp, idiag
        integer n_eigenvalues

        real*4  cov(maxstat,maxstat)
        real*4  icov(maxstat,maxstat)
        real*4  ident(maxstat,maxstat)
        real*4  ident_U(maxstat,maxstat)
        real*4  ident_V(maxstat,maxstat)
        real*4  a(maxstat,maxstat)
        real*4  b(maxstat,maxstat)
        real*4  b_inv(maxstat,maxstat)
        real*4  u(maxstat,maxstat)
        real*4  u_T(maxstat,maxstat)
        real*4  v(maxstat,maxstat)
        real*4  v_T(maxstat,maxstat)
        real*4  w(maxstat)
        real*4  q, q_inv

        real*4 diag(maxstat),sigma(maxstat)
        real*4 max_value, min_value, prev_value
        real*4 zdum



        do isamp=1,nsamp
           diag(isamp)=0.0
           do jsamp=1,nsamp
             icov(isamp,jsamp)=0.0
             ident(isamp,jsamp)=0.0
             b(isamp,jsamp)=0.0
             b_inv(isamp,jsamp)=0.0
          enddo   
        enddo

        nsamp2=nsamp

        do isamp=1,nsamp2
          do jsamp=1,nsamp2
             a(isamp,jsamp)= cov(isamp,jsamp)
             u(isamp,jsamp)= cov(isamp,jsamp)
          enddo
        enddo

        call svdcmp(u,nsamp2,nsamp2,maxstat,maxstat,w,v)


        n_eigenvalues=0
        max_value=0.0
        prev_value=1.e10
c Finding maximum eigenvalue
         do isamp=1,nsamp2
c           write(*,'(i6,f18.12)') isamp, w(isamp)
c           if(w(isamp).gt.prev_value)write(*,*)                            ' WARN:: not-ordered EIGENVALUES!'
           if(w(isamp).gt.max_value) then
              max_value=w(isamp)
           endif
              prev_value=w(isamp)
        enddo

        write(*,*) ' Found MAXIMUM eigenvalue: ',max_value
        min_value=max_value*min_eigenvalue

        do isamp=1,nsamp2
           if(w(isamp).lt.min_value) then
              w(isamp)=min_value
           else
              n_eigenvalues = n_eigenvalues + 1
           endif
        enddo

        write(*,*) ' Found ',n_eigenvalues,  ' NOT-MINIMUM eigenvalues ...'


        do isamp=1,nsamp2
          do jsamp=1,nsamp2
             v_T(isamp,jsamp)=v(jsamp,isamp)
             u_T(isamp,jsamp)=u(jsamp,isamp)
          enddo
        enddo


c
c Construct INVERSE COVARIANCE MATRIX -- using ONLY V matrix (becasue it MUST BE symmetric!!!!)
c
c Re-construct COVARIANCE MATRIX (i.e. the Covariance matrix which is exactly the inverse of the inverse covariance matrix)
c -- using ONLY V matrix (because it MUST BE symmetric!!!!)

        do isamp=1,nsamp2
          do jsamp=1,nsamp2
             if(w(jsamp).gt.0.0)b_inv(isamp,jsamp)=v(isamp,jsamp)*(1/w(jsamp))
             if(w(jsamp).gt.0.0)b(isamp,jsamp)=v(isamp,jsamp)*w(jsamp)
          enddo
        enddo        
        do isamp=1,nsamp2
          do jsamp=1,nsamp2
             q = 0.0
             q_inv=0.0
            do ksamp=1,nsamp2
               q = q + b(isamp,ksamp)*v_T(ksamp,jsamp)
               q_inv = q_inv + b_inv(isamp,ksamp)*v_T(ksamp,jsamp)
            enddo
            icov(isamp,jsamp) = q_inv
            cov(isamp,jsamp) = q
          enddo
        enddo
    

c$$$        write(*,*) 'COV matrix: '
c$$$        do isamp=1,nsamp
c$$$          write(*,'(41(g10.3))') (cov(isamp,jsamp),jsamp=1,nsamp)
c$$$        enddo
c$$$        write(*,*) 'ICOV matrix: '
c$$$        do isamp=1,nsamp
c$$$          write(*,'(41(g10.3))') (icov(isamp,jsamp),jsamp=1,nsamp)
c$$$        enddo
        
        do isamp=1,nsamp
          do jsamp=1,nsamp
             ident(isamp,jsamp)=0.0
             ident_U(isamp,jsamp)=0.0
             ident_V(isamp,jsamp)=0.0
             do ksamp=1,nsamp
                ident(isamp,jsamp)=ident(isamp,jsamp) + 
     &                         (icov(isamp,ksamp)*cov(ksamp,jsamp))
                ident_U(isamp,jsamp)=ident_U(isamp,jsamp) + 
     &                         (u(isamp,ksamp)*u_T(ksamp,jsamp))
                ident_V(isamp,jsamp)=ident_V(isamp,jsamp) + 
     &                         (v(isamp,ksamp)*v_T(ksamp,jsamp))
             enddo
          enddo
        enddo

c$$$        write(*,*) ' IDENTITY matrix '
c$$$        do isamp=1,nsamp
c$$$          write(*,'(41(g10.3))') (ident(isamp,jsamp),jsamp=1,nsamp)
c$$$        enddo
c$$$        write(*,*) ' IDENTITY matrix '
c$$$        do isamp=1,nsamp
c$$$          write(*,'(41(g10.3))') (ident_U(isamp,jsamp),jsamp=1,nsamp)
c$$$        enddo
c$$$        write(*,*) ' IDENTITY matrix '
c$$$        do isamp=1,nsamp
c$$$          write(*,'(41(g10.3))') (ident_V(isamp,jsamp),jsamp=1,nsamp)
c$$$        enddo


                

        return
        end
