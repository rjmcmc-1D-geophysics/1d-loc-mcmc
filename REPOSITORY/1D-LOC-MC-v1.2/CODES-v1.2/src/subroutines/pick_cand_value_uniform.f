c***************************************************************************
c**
c**  This file is part of 1D-LOC-MCMC project
c**
c**  1D-LOC-MCMC is free software: you can redistribute it and/or modify
c**  it under the terms of the GNU General Public License as published by
c**  the Free Software Foundation, either version 3 of the License, or
c**  (at your option) any later version.
c**
c**  1D-LOC-MCMC is distributed in the hope that it will be useful,
c**  but WITHOUT ANY WARRANTY; without even the implied warranty of
c**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c**  GNU General Public License for more details.
c**
c**  You should have received a copy of the GNU General Public License
c**  along with the code.  If not, see <http://www.gnu.org/licenses/>
c**
c**  See also https://gitlab.com/rjmcmc-1D-geophysics/1d-loc-mcmc
c**
c**  Created: 2024-07-01
c**  Copyright: 2024-2032
c**    Nicola Piana Agostinetti
c**
c***************************************************************************/

      subroutine pick_cand_value_uniform(x,xmin,xmax,sc,x_new)

      implicit none

      include '../mcmc.param'
      include '../mcmc.common'


      real*4 x,xmin,xmax,sc,xcand, x_new
      real*4 halfwidth, xcandhigh, xcandlow
      real*4 probacc, xhigh, xlow
      real*4 maxdev, dev


c RAN3 variables
      real*4 ran3


      x_new=x
      halfwidth=0.5*sc*(xmax-xmin)
      xcandhigh=x+halfwidth
      if(xcandhigh.gt.xmax)xcandhigh=xmax
      xcandlow=x-halfwidth
      if(xcandlow.lt.xmin)xcandlow=xmin
      maxdev=xcandhigh-xcandlow
      dev=ran3(iseed0)
      xcand=xcandlow+dev*maxdev
      xhigh=xcand+halfwidth
      if(xhigh.gt.xmax)xhigh=xmax
      xlow=xcand-halfwidth
      if(xlow.lt.xmin)xlow=xmin
      probacc=(xcandhigh-xcandlow)/(xhigh-xlow)
      dev=ran3(iseed0)
      if(probacc.ge.dev)then
        x_new=xcand
      endif

      return
      end
