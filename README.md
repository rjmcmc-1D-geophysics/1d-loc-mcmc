# 1D-LOC-MCMC


THIS REPOSITORY CONTAINS THE SOURCE CODE for publication:

AUTHORS: Riva, F., N. Piana Agostinetti, S. Marzorati and C. Horan

TITLE: The micro-seismicity of Co. Donegal (Ireland): Defining baseline seismicity in a region of slow lithospheric deformation

JOURNAL: Terra Nova  

YEAR: 2024 

DOI: 10.1111/ter.12691

The repository contains a FORTRAN source code which implements the algorithm presented in the publication, a Markov chain Monte Carlo algorithm for the location of a seismic event in an half-space. All innovation, formulas and details are given in the publication. The code can be used to reproduce all the results obtained in the paper, for one of the real-world cases presented in the manuscript, namely the location of the natural event #31, presented in Figure 6 of the above manuscript. 